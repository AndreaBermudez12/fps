﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverManager : MonoBehaviour
{
    public void PulsaExit()
    {

        Application.Quit();
    }
    public void PulsaTry()
    {

        SceneManager.LoadScene("MainMenu");
    }
}
