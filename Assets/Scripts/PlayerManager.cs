﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerManager : MonoBehaviour
{

        private Vector2 axis;
        private CharacterController controller;
        public float speed;
        public Vector3 moveDirection;
        public float jumpSpeed;
        private bool jump;
        public float gravityMagnitude = 1.0f;
        public int hp = 20;
        private bool iamDead = false;

        private Vector3 transformDirection;
        public GameObject mano;
        public GameObject lanzacohetes;
        public AudioSource disparo;

        [SerializeField] GameObject ps;
        [SerializeField] AudioSource Dolor;
        [SerializeField] private FireTemplate bullet;
        [SerializeField] private FireTemplate rocketBullet;
        [SerializeField] ScoreCanvas scoreCanvas;

        void Start()
        {
            controller = GetComponent<CharacterController>();
        }

        void Update()
        {
            transformDirection = axis.x * transform.right + axis.y * transform.forward;

            moveDirection.x = transformDirection.x * speed;
            moveDirection.z = transformDirection.z * speed;

            if (controller.isGrounded && !jump)
            {
                moveDirection.y = Physics.gravity.y * gravityMagnitude * Time.deltaTime;
            }
            else
            {
                jump = false;
                moveDirection.y += Physics.gravity.y * gravityMagnitude * Time.deltaTime;
            }


            controller.Move(moveDirection * Time.deltaTime);
        }


        public void SetAxis(Vector2 inputAxis)
        {
            axis = inputAxis;
        }

        public void Fire()
        {
           // Debug.Log("Fire");

            // Instanciar una pelota
            FireTemplate pelota = Instantiate(bullet, mano.transform.position, mano.transform.rotation) as FireTemplate;

            // Ponerla en la posición del player
            disparo.Play();
            // Dispararla
            pelota.Fire();
        }

        public void Fire2()
        {
            //Debug.Log("Fire2");

            // Instanciar una pelota
            FireTemplate misil = Instantiate(rocketBullet, lanzacohetes.transform.position, lanzacohetes.transform.rotation) as FireTemplate;

            // Ponerla en la posición del player
            disparo.Play();
            // Dispararla
            misil.Fire();
        }

        public void Jump()
        {
            if (!controller.isGrounded) return;

            moveDirection.y = jumpSpeed;
            jump = true;
        }

        public void OnTriggerEnter(Collider other)
        {

            //Debug.Log("Colision");
            if (other.tag == "Enemy"){
                StartCoroutine(Golpe());
            }
       
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.tag == "control")
            {
                SceneManager.LoadScene("Victoria");
            }
        }

        IEnumerator Golpe()
        {
            //Indico que estoy muerto
            iamDead = true;
            //Indicamos al score que hemos perdido una vida
            scoreCanvas.LoseLife();
            Dolor.Play();
            Instantiate(ps, transform.position, transform.rotation, null);
            //Me espero 1 segundo
            yield return new WaitForSeconds(2.0f);
            iamDead = false;


        }

    }
