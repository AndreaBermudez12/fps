﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public void PulsaPlay()
    {
        //Debug.LogError("He pulsado Play");

        SceneManager.LoadScene("TaChido");
    }

    public void PulsaExit()
    {
        Application.Quit();
    }

    public void PulsaCredits()
    {
        SceneManager.LoadScene("Credits");
    }

    public void PulsaHowtoplay()
    {
        SceneManager.LoadScene("Howtoplay");
    }
}
