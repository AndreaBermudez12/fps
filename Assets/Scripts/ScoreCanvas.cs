﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScoreCanvas : MonoBehaviour
{
    [SerializeField] GameObject[] lives;
    [SerializeField] TextMeshProUGUI score;
    [SerializeField] AudioSource puntos;

    private int scoreInt = 0;
    private int currentLives = 3;

    void Start()
    {
        scoreInt = 0;
        currentLives = 3;
        score.text = "0";// scoreInt.ToString("000000");
        //score.text = scoreInt.ToString();
    }
    void Update()
    {
        if(currentLives == 0)
        {
           SceneManager.LoadScene("GameOver");
        }

        /* if(scoreInt == 20)
         {
             SceneManager.LoadScene("Victoria");
         }*/
    }
    public void LoseLife()
    {
        Debug.Log("pierdo vida");
        currentLives--;
        lives[currentLives].SetActive(false);
            
       
    }

    public void Matazombies()
    {
        scoreInt++;
        score.text = scoreInt.ToString();
        puntos.Play();
    }
    }
