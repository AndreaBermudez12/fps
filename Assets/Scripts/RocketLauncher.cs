﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketLauncher : FireTemplate
{
    //public int damage = 3;
    public int explosionForce = 20;
    public float throughForce;
    public bool launched = false;
    public GameObject explosionPrefab;
    public override void DestroySelf()
    {
        Explosion();
    }

    public override void Fire()
    {
        launched = true;        
    }


    private void FixedUpdate()
    {
        if (launched)
        {
            GetComponent<Rigidbody>().AddForce(throughForce * transform.forward, ForceMode.Force);
        }
    }

     private void OnCollisionEnter(Collision collision)
     {
         if (collision.gameObject.tag == "Enemy")
         {
             //Debug.Log("He tocado un enemy");
             //collision.gameObject.GetComponent<CreaperBehaviour>().Damage(damage);
             Rigidbody rb = collision.gameObject.GetComponent<Rigidbody>();
             if (rb)
             {
                 rb.AddForce(explosionForce*transform.forward, ForceMode.Impulse);
             }
         }

        if (collision.gameObject.tag == "Terrain")
        {
            Destroy(this.gameObject);
        }

        Explosion();
     }


    private void Explosion()
    {

        //Instanciar una explosion
       // Instantiate(explosionPrefab, transform.position, transform.rotation);

        //Destruirse
        Destroy(this.gameObject);
    }
}
