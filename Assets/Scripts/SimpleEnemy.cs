﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleEnemy : MonoBehaviour
{
    public Collider[] patrolPoints;
    private int currentPoint = 0;
    // Use this for initialization
    private UnityEngine.AI.NavMeshAgent agent;
    public int hp = 6;


    void Start()
    {
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        agent.SetDestination(patrolPoints[currentPoint].gameObject.transform.position);
    }

    void Update()
    {
        if (hp == 0)
        {
            Destroy(this.gameObject);
        }

    }

    void OnTriggerEnter(Collider col)
    {
        currentPoint++;
        currentPoint = currentPoint % patrolPoints.Length;
        agent.SetDestination(patrolPoints[currentPoint].gameObject.transform.position);

        //Debug.LogError("Colisiono");
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {
            hp = hp - 3;
        }

    }
}
